import React from 'react';
import { Route, Switch } from 'dva/router';
import BaseRouter from 'react-router-dom/BrowserRouter';
import Home from './routes/Home';
import HotAreaTool from './routes/HotAreaTool';

function RouterConfig({ history }) {
  return (
    <BaseRouter>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/hotAreaTool" component={HotAreaTool} />
      </Switch>
    </BaseRouter>
  );
}

export default RouterConfig;
