import React from 'react';
import HotArea from '../components/HotArea';
import HotPic from '../components/HotPic';
import ShowArea from '../components/ShowArea';
import CanvasArea from '../components/CanvasArea';
import { Icon } from 'antd';
import { Input } from 'antd';
import { InputNumber } from 'antd';
import 'antd/dist/antd.css';

const styles = {
    wrap: {
        width: '60%',
        height: '100%',
        textAlign: 'center',
        margin: 'auto',
        display: 'flex',
        justifyContent: 'space-between',
        backgroundColor: 'rgb(238,238,238)',
        // border: '0.2rem solid red',
    },
    left: {
        width: '30%',
        height: 1000,
    },
    leftTitle: {
        width: '100%',
        height: 50,
        cursor: 'pointer',
    },
    leftContentTitle: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        width: '100%',
        border: '0.01rem solid gray',
        margin: 'auto',
    },
    leftContent: {
        display: 'flex',
        justifyContent: 'space-around',
        width: '100%',
        height: '80%',
        border: '0.01rem solid gray',
        borderTop: '0.01rem solid transparent',
    },
    middle: {
        width: '39%',
        height: 1000,
    },
    middleTitle: {
        width: '100%',
        height: 50,
    },
    middleContent: {
        width: '100%',
        height: '80%',
        backgroundColor: '#fff',
    },
    right: {
        width: '30%',
        height: 1000,
    },
    rightTitle: {
        width: '100%',
        height: 50,
    },
    rightContent: {
        width: '100%',
        height: '80%',
    },
    rightContent_bottom: {
        width: '100%',
        height: '50%',
        border: '1px solid gray',
    },
    span: {
        display: 'inline-block',
        width: '66px',
        height: '24px',
        cursor: 'pointer',
        color: '#108ee9',
        fontSize: '16px',
        outline: '1px solid #108ee9',
        textAlign: 'center',
    }
}

class HotAreaToolShow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showHotAreaOrPic: 0,
            isShowArea: false,
            propWhich: 0,
            isShowCanvasArea: 0,
            CanvasAreaStyle: {}
        }
        this.hotUrl = "//m.taobao.com/";
        this.hotPicUrl = "//img.alicdn.com/imgextra/i3/3355153118/TB2zuVRv1uSBuNjSsplXXbe8pXa-3355153118.png";
        this.argArr = [];
        this.value = '';
        this.obj = {};
    }

    addHotUrl = () => {
        this.setState({ isShowArea: true, propWhich: 1, isShowCanvasArea: 1 })
    }

    addHotPicUrl = () => {
        this.setState({ isShowArea: true, propWhich: 2, isShowCanvasArea: 1 })
    }

    getChildrenData = (arg = {}) => {
        if (arg.x) {
            this.obj.left = arg.x + "px";
        }
        if (arg.y) {
            this.obj.top = arg.y + "px";
        }
        if (arg.w) {
            this.obj.width = arg.w + "px";
        }
        if (arg.h) {
            this.obj.height = arg.h + "px";
        }
        if (arg.url) {
            this.obj.url = arg.url;
            this.hotPicUrl = arg.url;
        }
        if (arg.screenH) {
            this.obj.screenHeight = arg.screenH;
        }
        if (arg.screenW) {
            this.obj.screenWidth = arg.screenW;
        }
        this.setState({ CanvasAreaStyle: this.obj });
        console.log("this.state.CanvasAreaStyle===", this.state.CanvasAreaStyle);
        console.log("this.obj", this.obj);
    }

    handleChangeTextAreaHotPicH = (value) => {
        console.log('handleChangeTextAreaHotPicH=====', value);
    }

    render() {
        const { TextArea } = Input;
        return (
            <div style={{ ...styles.wrap }}>
                <div style={{ ...styles.left }}>
                    <div style={{ ...styles.leftTitle }}>|图层</div>
                    <div style={{ ...styles.leftContentTitle }}>
                        <div onClick={() => { this.setState({ showHotAreaOrPic: 1 }) }} style={{ ...styles.leftTitle }}>热区</div>
                        <div onClick={() => { this.setState({ showHotAreaOrPic: 2 }) }} style={{ ...styles.leftTitle }}>图片</div>
                    </div>
                    <div style={{ ...styles.leftContent }}>
                        {
                            this.state.showHotAreaOrPic === 1 ? <HotArea /> : <HotPic />
                        }
                    </div>
                </div>
                <div style={{...styles.middle/*,backgroundColor: 'red',width: this.obj.screenWidth*/}}>
                    <div style={{ ...styles.middleTitle }}>|画布区</div>
                    <div style={{ ...styles.middleContent }}>
                        {
                            this.state.isShowCanvasArea === 1 ? <CanvasArea hotPicUrl={this.hotPicUrl} style={this.state.CanvasAreaStyle} url={this.state.propWhich === 1 ? this.hotUrl : this.hotPicUrl} /> : null
                        }
                    </div>
                </div>
                <div style={{ ...styles.right }}>
                    <div style={{ ...styles.rightTitle }}>|操作区</div>
                    <div style={{ ...styles.rightContent }}>
                        <div style={{ ...styles.rightContent_top }}>
                            <div style={{ margin: 10 }}>
                                屏幕高度:
                                <InputNumber defaultValue={1334} onBlur={(e)=>{this.getChildrenData({screenH: e.target.value})}} min={1} max={100000} size={'small'} />
                            </div>
                            <div style={{ margin: 10 }}>
                                屏幕宽度:
                                <InputNumber defaultValue={750} onBlur={(e)=>{this.getChildrenData({screenW: e.target.value})}} min={1} max={100000} size={'small'} />
                            </div>
                            <div style={{ display: 'flex', justifyContent: 'space-around', margin: 20 }}>
                                <span onClick={() => { this.addHotUrl() }}>
                                    <Icon type="scan" theme="outlined" style={{ fontSize: '24px' }} />
                                    <p>添加热区</p>
                                </span>
                                <span onClick={() => { this.addHotPicUrl() }}>
                                    <Icon type="picture" theme="outlined" style={{ fontSize: '24px' }} />
                                    <p>添加图片</p>
                                </span>
                            </div>
                            <div className="showArea">
                                {
                                    this.state.isShowArea ? <ShowArea getChildrenData={this.getChildrenData} url={{ hotUrl: this.hotUrl, hotPicUrl: this.hotPicUrl }} /> : <div style={{ width: '100%', height: 200 }}></div>
                                    // <ShowArea />
                                }
                            </div>
                        </div>
                        <div style={{ ...styles.rightContent_bottom }}>
                            <h4 style={{ backgroundColor: 'rgb(238,238,238)', width: '100%', height: 50 }}>
                                请复制下面代码到表单！
                                <span style={{ ...styles.span }}>不可编辑</span>
                            </h4>
                        <TextArea value={this.obj.left ? '{"screenHeight":'+this.obj.screenWidth+',"screenWidth":'+this.obj.screenHeight+',"hot":[],"img":['+JSON.stringify(this.obj)+']}' : '{"screenHeight":1334,"screenWidth":750,"hot":[],"img":[]}'} onChange={this.handleChangeTextAreaHotPicH} rows={16} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default HotAreaToolShow;