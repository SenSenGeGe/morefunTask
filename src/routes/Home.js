import React from 'react';
import { connect } from 'dva';
import styles from './Home.css';
import HomeShow from '../components/HomeShow';

function Home() {
    return (
        <div className={styles.normal}>
            <HomeShow />
        </div>
    );
}

Home.propTypes = {
};

// export default Home;
export default connect(({ home }) => ({
    home,
}))(Home);
