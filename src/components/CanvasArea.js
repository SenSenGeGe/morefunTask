import React from 'react';

class CanvasArea extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            picArr: [],
        }
    }

    addPic = () => {
        let newPicArr = this.state.picArr.push(this.props.url);
        this.setState({ picArr: newPicArr });
    }

    changeUrl = () => {
        // let imageSrc = document.getElementById('images').width;
        // return imageSrc;
    }

    render(){
        console.log('33333',this.props);
        return (
            <div style={{position: 'relative'}}>
                <img id="images" src={this.props.hotPicUrl} alt="" style={{
                    width: "200px",
                    height: "200px",
                    top: 0,
                    left: 0,
                    position: 'absolute',
                    ...this.props.style
                }} />
            </div>
        );
    }
}

export default CanvasArea;