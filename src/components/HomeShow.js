import React from 'react';
import styles from '../routes/Home.css';
import { Link } from 'react-router-dom';

const HomeShow = () => {
    return (
        <div className={styles.normal}>
            <ul className={styles.list}>
                <h1><Link to="/hotAreaTool">热区工具</Link></h1>
                <h1><Link to="/slider">轮播工具</Link></h1>
            </ul>
        </div>
    );
}

export default HomeShow;