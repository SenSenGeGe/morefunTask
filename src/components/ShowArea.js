import React from 'react';
import { InputNumber, Input } from 'antd';
import 'antd/dist/antd.css';

class ShowArea extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showPicOrHotarea: 2,
            hotPicUrl: this.props.url.hotPicUrl,
            hotPicX: '',
        }
        this.X = '';
        this.Y = '';
        this.W = '';
        this.H = '';
    }

    handleChangeHotPicUrl = (e) => {
        this.setState({hotPicUrl: e.target.value});
        console.log('222222',this.state.hotPicUrl);
      }

      handleChangeHotUrl = (e) => {
        this.setState({hotPicUrl: e.target.value});
      }

      handleChangeHotPicX = (value) => {
        this.X = value;
        console.log('X=====',this.X);
      }

      handleChangeHotPicY = (value) => {
        console.log('hotPicY=====',value);
        this.argArrY.push(value);
      }

      handleChangeHotPicW = (value) => {
        console.log('hotPicW=====',value);
        this.argArrW.push(value);
      }

      handleChangeHotPicH = (value) => {
        console.log('hotPicH=====',value);
        this.argArrH.push(value);
      }

    render() {
        return (
            this.state.showPicOrHotarea===1 ? <div>
                <p style={{ fontSize: '28px' }}>展示区域</p>
                <div style={{ margin: 10 }}>
                    x:<InputNumber min={1} max={100000} size={'small'} />
                    y:<InputNumber min={1} max={100000} size={'small'} />
                </div>
                <div style={{ margin: 10 }}>
                    w:<InputNumber min={1} max={100000} size={'small'} />
                    h:<InputNumber min={1} max={100000} size={'small'} />
                </div>
                <div style={{ margin: 20, justifyContent: 'start' }}>
                    url:<Input value={this.state.hotUrl} onChange={this.handleChangeHotUrl} min={1} max={100000} size={'small'} />
                </div>
            </div> : <div>
                <p style={{ fontSize: '28px' }}>展示区域</p>
                <div style={{ margin: 10 }}>
                    x:<InputNumber defaultValue={0} onBlur={(e)=>{this.props.getChildrenData({x: e.target.value})}} min={1} max={100000} size={'small'} />
                    y:<InputNumber defaultValue={0} onBlur={(e)=>{this.props.getChildrenData({y: e.target.value})}} min={1} max={100000} size={'small'} />
                </div>
                <div style={{ margin: 10 }}>
                    w:<InputNumber defaultValue={200} onBlur={(e)=>{this.props.getChildrenData({w: e.target.value})}} min={1} max={100000} size={'small'} />
                    h:<InputNumber defaultValue={200} onBlur={(e)=>{this.props.getChildrenData({h: e.target.value})}} min={1} max={100000} size={'small'} />
                </div>
                <div style={{ margin: 20, justifyContent: 'start' }}>
                    url:<Input defaultValue={'//img.alicdn.com/imgextra/i3/3355153118/TB2zuVRv1uSBuNjSsplXXbe8pXa-3355153118.png'} onBlur={(e)=>{this.props.getChildrenData({url: e.target.value})}} size={'small'} />
                </div>
            </div>
        );
    }
}

export default ShowArea;