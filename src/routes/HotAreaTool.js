import React from 'react';
import { connect } from 'dva';
import HotAreaToolShow from '../components/HotAreaToolShow';

function HotArea() {
    return (
        <div>
            <HotAreaToolShow />
        </div>
    );
}

export default connect(({ hotArea }) => ({
    hotArea,
}))(HotArea);